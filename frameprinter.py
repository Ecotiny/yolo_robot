def printframe(objs):
    width = 40 * 2
    height = 15 * 2# 3:2 / 2 because hack is 2:1
    frame = [[" " for _ in range(width)] for _ in range(height)]
    for obj in objs:
        x1, y1, x2, y2 = obj['box']
        x1 = int(round(x1 * width))
        y1 = int(round(y1 * height))
        x2 = int(round(x2 * width))
        y2 = int(round(y2 * height))
        for y in range(height):
            for x in range(width):
                if x in [x1, x2] and y in [y1, y2]:
                    frame[y][x] = "+"
                elif x in [x1, x2] and y1 < y < y2:
                    frame[y][x] = "|"
                elif y in [y1, y2] and x1 < x < x2:
                    frame[y][x] = "-"
                elif y == int(round(0.8 * height)):
                    frame[y][x] = "-"
        
    print("+" + ("-" * width) + "+")
    for row in frame:
        print("|" + ("".join(row)) + "|")
    print("+" + ("-" * width) + "+")
    return

if __name__ == "__main__":
    printframe({"CAN":[(0.1, 0.1), (0.9, 0.9)]})
