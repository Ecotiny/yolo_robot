import run_multiprocessing
import multiprocessing
import cv2 as cv
import time
import numpy as np
import json

# simulate camera by feeding it images from a directory

class DummyCameraSupervisor(multiprocessing.Process):
    
    def __init__(self, ld_queue, yd_queue, sv_queue, directory, yd_debug_queue, green_debug_queue):
        multiprocessing.Process.__init__(self)
        self.ld_queue = ld_queue
        self.yd_queue = yd_queue
        self.sv_queue = sv_queue
        self.directory = directory
        self.yd_debug_queue = yd_debug_queue

    def run(self):
        if self.directory.endswith(".mp4"):
            cap = cv.VideoCapture(self.directory)
        else:
            cap = cv.VideoCapture(self.directory + "/image%09d.jpg")
        while True:
            time.sleep(0.05)

            ret, image = cap.read()
            
            if ret == True:
                cv.imshow("Video Frame", image)
                if not yd_debug_queue.empty():
                    cv.imshow("Detected", yd_debug_queue.get())
                if not green_debug_queue.empty():
                    cv.imshow("Turn Detected", green_debug_queue.get())
                cv.waitKey()
                
                if self.yd_queue.empty():
                    self.yd_queue.put(image)
                if self.ld_queue.empty():
                    self.ld_queue.put(image)
                if self.sv_queue.empty():
                    self.sv_queue.put(image)
            else:
                break

class DummyPID(multiprocessing.Process):
    
    def __init__(self, svpid_queue): # supervisor - PID class
        multiprocessing.Process.__init__(self)
        self.svpid_queue = svpid_queue
        self.max_throttle = 100
        self.printjson = False
        
    def run(self):
        while True:
            time.sleep(0.01)
            if not self.svpid_queue.empty():
                x_target = self.svpid_queue.get()
                if type(x_target) == float or type(x_target) == int or type(x_target) == np.float64:
                    bangbangthresh = 0.2
                    left_motor = self.max_throttle
                    right_motor = self.max_throttle
                    if x_target > bangbangthresh:
                        left_motor = self.max_throttle
                        right_motor = 0
                    elif x_target < -bangbangthresh:
                        right_motor = self.max_throttle
                        left_motor = 0
                    
                    if self.printjson:
                        data = {"left": left_motor, "right": right_motor, "r" : -1}
                        json_string = json.dumps(data)
                        print(json_string)

                    self.prev_rthrottle = right_motor
                    self.prev_lthrottle = left_motor
                elif type(x_target) == list:
                    left_motor, right_motor = x_target
                    
                    if self.printjson:
                        data = {"left": left_motor, "right": right_motor, "r" : -1}
                        json_string = json.dumps(data)
                        print(json_string)
                        
                    #print("{: 03} {: 03}".format(left_motor, right_motor))
                    self.prev_rthrottle = right_motor
                    self.prev_lthrottle = left_motor
                elif x_target == "STOP":
                    print("STOPPING")
                    # stop
                    if self.printjson:
                        data = {"left": left_motor, "right": right_motor, "r" : -1}
                        json_string = json.dumps(data)
                        print(json_string)

                    #print("{: 03} {: 03}".format(0, 0))
                else:
                    if self.printjson:
                        data = {"left": self.prev_lthrottle, "right": self.prev_rthrottle, "r" : -1}
                        json_string = json.dumps(data)
                        print(json_string)
                    #print("{: 03} {: 03}".format(self.prev_lthrottle, self.prev_rthrottle))


if __name__ == "__main__":
    
    sv_queue = multiprocessing.Queue()
    ld_queue = multiprocessing.Queue()
    yd_queue = multiprocessing.Queue()
    svld_queue = multiprocessing.Queue()
    svyd_queue = multiprocessing.Queue()
    svpid_queue = multiprocessing.Queue()
    yd_debug_queue = multiprocessing.Queue()
    green_debug_queue = multiprocessing.Queue()
    
    cv.namedWindow('Video Frame')
    cv.namedWindow("Detected")
    cv.namedWindow("Turn Detected")
    
    print("Initalising yolo detector")
    # Initialise YOLO Detector
    yd = multiprocessing.Process(target=run_multiprocessing.YOLODetector, args=(yd_queue, svyd_queue, True, yd_debug_queue))
    yd.start()

    # initialise camera supervisor
    cam_supervisor = DummyCameraSupervisor(ld_queue, yd_queue, sv_queue, "video_h_002/", yd_debug_queue, green_debug_queue)
    cam_supervisor.start()

    print("WAITING")
    time.sleep(5)
    print("STARTING")


    # initialise the supervisor and PID
    supervisor = run_multiprocessing.Supervisor(sv_queue, svld_queue, svyd_queue, svpid_queue, green_debug_queue)
    supervisor.start()
    PID_ctler = DummyPID(svpid_queue)
    PID_ctler.start()


    # Initialise Line Detectors
    ld = run_multiprocessing.LineDetector(ld_queue, svld_queue)
    ld.start()

    ld.join()
    yd.join()
    PID_ctler.join()
    supervisor.join()
    cam_supervisor.join()

