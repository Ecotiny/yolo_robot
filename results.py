import numpy as np
import cv2 as cv

class Results:
    def __init__(self, out_boxes, out_scores, out_classes, image):
        self.objects = []
        self.class_names = ["CAN", "TURN", "WATER"]
        self.objclasses = []
        for i, c in reversed(list(enumerate(out_classes))):
            predicted_class = self.class_names[c]
            box = out_boxes[i]
            score = out_scores[i]

            obj = DetectedObject(predicted_class, box, score, image)
            self.objclasses.append(obj)
            self.objects.append(obj.to_dict())


class DetectedObject:
    def __init__(self, detectedtype, box, score, img):
        self.score = score
        y1, x1, y2, x2 = box
        self.box = [x1, y1, x2, y2]
        self.roi = None
        self.detectedtype = detectedtype
        
        if detectedtype == "TURN":
            self.colour = (0, 255, 0)
        elif detectedtype == "WATER":
            self.colour = (255, 0, 0)
        elif detectedtype == "CAN":
            self.colour = (200, 200, 200)

        if detectedtype == "TURN":
            # convert 0 -> 1 to 0 -> image width/height
            imheight, imwidth, _ = img.shape
            x1 = int(round(x1 * imwidth))
            y1 = int(round(y1 * imheight))
            x2 = int(round(x2 * imwidth))
            y2 = int(round(y2 * imheight))

            # cut out roi
            self.roi = img[y1:y2, x1:x2]

    def to_dict(self):
        ret = {'confidence' : self.score,
               'box'        : self.box,
               'roi'        : self.roi,
               'type'       : self.detectedtype}
        return ret
    
    def draw(self, img):
        x1, y1, x2, y2 = self.box
        
        imheight, imwidth, _ = img.shape
        
        x1 = int(round(x1 * imwidth))
        y1 = int(round(y1 * imheight))
        x2 = int(round(x2 * imwidth))
        y2 = int(round(y2 * imheight))
                
        cv.rectangle(img, (x1, y1), (x2, y2), self.colour, 2)

