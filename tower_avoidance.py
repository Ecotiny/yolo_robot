from run_multiprocessing import PID
import state_handler
from frameprinter import printframe
import multiprocessing
import time
import argparse

if __name__ == "__main__":
    svpid_queue = multiprocessing.Queue()

    parser = argparse.ArgumentParser(description='Test things')


    parser.add_argument('--proportional',
                        default=10,
                        type=float,
                        help='Proportional gain of PID controller')

    parser.add_argument('--integral',
                        default=0.1,
                        type=float,
                        help='Integral gain for PID controller')

    parser.add_argument('--derivative',
                        default=0.05,
                        type=float,
                        help='Derivative gain for PID controller')

    args = parser.parse_args()

    sh = state_handler.StateHandler(svpid_queue)
    sh.state = state_handler.STATE_WATERTOWER

    PID_ctler = PID(svpid_queue, 100, args.proportional, args.integral, args.derivative)
    PID_ctler.start()
    
    input("Press enter to start")

    while sh.state == state_handler.STATE_WATERTOWER:
        time.sleep(0.05)
        sh.handle_watertower()
    print("excitingiasdf")
    
    sh.handle_stop()

    print("done")
    
    time.sleep(0.1)
    PID_ctler.join()
