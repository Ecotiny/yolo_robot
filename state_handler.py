import process
from leftotron import LeftyMcRightface
from blobby import Blobby
import serial
import json
import cv2 as cv
import numpy as np

STATE_START = 0
STATE_LINEFOLLOWER = 10
STATE_TURNLEFT = 20
STATE_TURNRIGHT = 30
STATE_WATERTOWER = 40
STATE_GRIDLOCK = 50
STATE_ONSPILL = 60
STATE_APPROACHINGCAN = 70
STATE_CANPICKUP = 80
STATE_BOXLOCATE = 90
STATE_PLACECAN = 100
STATE_STOP = 110
STATE_LOST = 120

class StateHandler:

    def __init__(self, svpid_queue, w, green_debug_queue=False):
        self.svpid_queue = svpid_queue
        self.lr = LeftyMcRightface(int(round(w/2)))
        self.blby = Blobby()
        self.w = w
        
        self.green_debug_queue = green_debug_queue
        
        self.ser = serial.Serial("/dev/ttyUSB0")


        self.state = STATE_START

        self.lines = None
        self.objects = None
        self.img = None

        # y thresholds for when to respond
        self.water_y_threshold = 0.6
        self.can_y_threshold = 0.5
        self.turn_y_threshold = 0.55

        # executing a tower avoidance turn
        self.avoid_tower_left = ([0] * 12) + ([100] * 2) + ([100] * 11) + ([100] * 20) + ([100] * 12) + ([100] * 2) + ([0] * 8)
        self.avoid_tower_right = ([100] * 12) + ([100] * 2) + ([0] * 11) + ([100] * 20) + ([0] * 12) + ([100] * 2) + ([100] * 8)
        self.towerindex = 0

        # executing a turn
        self.turncounter = 0
        self.turntime = 10 # number of call cycles at 20Hz
        self.turnwaittime = 5
        self.turnend = False
        self.turnwaiting = False
        
        # executing a can search
        self.can_search_left = ([0] * 3) + ([0] * 10) + ([100] * 6) + ([0] * 10)
        self.can_search_right = ([100] * 3) + ([0] * 10) + ([0] * 6) + ([0] * 10)
        self.can_index = 0
        
        # variables for state scores
        self.Ctl_avg = [0] * 5
        self.Ctr_avg = [0] * 5
        self.Cw_avg  = [0] * 5
        self.Cc_avg  = [0] * 5 # running avg on certainty of can
        self.Ya      = 0 # age of most recent yolo data
        self.Ntl     = 0 # number of times a left turn has been below the threshold in the last 5 unique yolo images
        self.Ntr     = 0
        self.Nw      = 0
        self.Nc      = 0
        
        # state score threshold
        self.can_threshold = 0.8
        self.turn_threshold = 0.8
        self.water_threshold = 0.8

    def shift(self, inarr, element):
        outarr = [element] + inarr[:-1]
        return outarr

    def handle_state(self, lines, objects, img, Ya):
        self.lines = lines
        self.objects = objects
        self.img = img
        self.Ya = Ya
        
        if self.state == STATE_START:
            self.handle_start()
        elif self.state == STATE_LINEFOLLOWER:
            self.handle_linefollower()
        elif self.state == STATE_WATERTOWER:
            self.handle_watertower()
        elif self.state == STATE_CANPICKUP:
            self.handle_canpickup()
        elif self.state == STATE_STOP:
            self.handle_stop()
        elif self.state == STATE_TURNLEFT:
            self.handle_leftturn()
        elif self.state == STATE_TURNRIGHT:
            self.handle_rightturn()
        else:
            raise RuntimeError("Invalid state")
        
        self.Ya += 1

    def enter_stop(self):
        data = {"left" : -1, "r" : 255, "g" : 0, "b" : 0}
        json_string = json.dumps(data)
        self.ser.write((json_string).encode("utf-8"))
        # stops the robot
        self.state = STATE_STOP
        return

    def enter_canpickup(self):
        # head straight for can
        data = {"left" : -1, "r" : 255, "g" : 127, "b" : 127}
        json_string = json.dumps(data)
        self.ser.write((json_string).encode("utf-8"))
        allowed_states = [STATE_LINEFOLLOWER];
        assert(self.state in allowed_states)
        self.state = STATE_CANPICKUP
        return

    def enter_linefollower(self):
        data = {"left" : -1, "r" : 255, "g" : 0, "b" : 255}
        json_string = json.dumps(data)
        self.ser.write((json_string).encode("utf-8"))
        allowed_states = [STATE_START, STATE_TURNLEFT, STATE_TURNRIGHT, STATE_WATERTOWER]
        assert(self.state in allowed_states)
        self.state = STATE_LINEFOLLOWER
        # update RGB LEDs or something
        return

    def enter_leftturn(self):
        data = {"left" : -1, "r" : 125, "g" : 0, "b" : 0}
        json_string = json.dumps(data)
        self.ser.write((json_string).encode("utf-8"))

        allowed_states = [STATE_LINEFOLLOWER]
        assert(self.state in allowed_states)
        self.state = STATE_TURNLEFT
        return

    def enter_rightturn(self):
        data = {"left" : -1, "r" : 0, "g" : 125, "b" : 0}
        json_string = json.dumps(data)
       self.ser.write((json_string).encode("utf-8"))


        allowed_states = [STATE_LINEFOLLOWER]
        assert(self.state in allowed_states)
        self.state = STATE_TURNRIGHT
        return

    def enter_watertower(self):
        data = {"left" : -1, "r" : 0, "g" : 0, "b" : 255}
        json_string = json.dumps(data)
        self.ser.write((json_string).encode("utf-8"))

        allowed_states = [STATE_LINEFOLLOWER]
        assert(self.state in allowed_states)
        self.state = STATE_WATERTOWER
        print("Going into water tower")
        # update RGB LEDs or something
        return

    def handle_rightturn(self):
        print("Turning right")
        if self.turncounter > 0:
            self.svpid_queue.put(1)
            self.turnend = True
            self.turncounter -= 1
        elif self.turnend:
            self.turnend = False
            self.enter_linefollower()
        else:
            self.turncounter = self.turntime

    def handle_leftturn(self):
        print("Turning left")
        if self.turncounter > 0:
            self.svpid_queue.put(-1)
            self.turnend = True
            self.turncounter -= 1
        elif self.turnend:
            self.turnend = False
            self.enter_linefollower()
        else:
            self.turncounter = self.turntime

    def handle_watertower(self):
        output = [self.avoid_tower_left[self.towerindex], self.avoid_tower_right[self.towerindex]]
        self.svpid_queue.put(output)
        print(output)
        self.towerindex += 1
        if self.towerindex >= len(self.avoid_tower_left):
            self.towerindex = 0
            self.enter_linefollower()
            print("Going from water tower to line follower")
            return

    def handle_start(self):
        # check for some signal, and that line is present
        if self.lines is not None:
            self.enter_linefollower()

    def handle_stop(self):
        # empty array means no motor speed or anything
        self.svpid_queue.put("STOP")

    def leftorright(self, centre, img):
        # run line detector on y value of centre on grayscale img
        
        x, y = centre
        roi = img[y+10:y+11, :] # roi slice one pixel high

        roi = cv.cvtColor(roi, cv.COLOR_BGR2GRAY)
        roi = np.divide(roi, np.amax(roi)) # make range between 0 and 1
        results = self.lr.get_left_right(roi[0])
        thresh = 0.2

        if self.green_debug_queue != False:
            cv.circle(img, centre, 20, 255, 2)

        if results['left_certainty'] > thresh and results['right_certainty'] > thresh:
            scaled_left = results['left_edge'] 
            scaled_right = results['right_edge'] 
            linecentre = (scaled_left + scaled_right) / 2
            if self.green_debug_queue != False:
                cv.circle(img, (scaled_left, y+10), 20, (0,0,255), 2)
                cv.circle(img, (scaled_right, y+10), 20, (0,255,0), 2)
                
            print(__name__ + ": detected a {} turn".format("right" if x > linecentre else "left"))
            
            if linecentre > x: # line is to right of centre of green. Left turn
                return -1
            else: # right turn
                return 1
        if self.green_debug_queue != False:
            self.green_debug_queue.put(img)

    def handle_linefollower(self):
        # check if there are any objects nearby that would encourage switching to turning, water tower avoidance, gridlock, etc.
        if self.objects is not None:
            # there are objects to check the size of
            # where the coordinates are 0 to 1 for x, 0 to 1 for y
            # check the width, or absolute difference between x values

            # highest confidence objects
            highest_can = {"confidence":0, "type":"CAN"}
            highest_tower = {"confidence":0, "type":"WATER"}
            highest_turn = {"confidence":0, "type":"TURN"}
            

            for obj in self.objects:
                detected_class = obj['type']
                x1, y1, x2, y2 = obj['box']
                width = abs(x1 - x2) # x1 - x2
                centre_x = width / 2

                # put the most confidence cans, water towers and turns into the necessary vars
                if y2 > self.can_y_threshold and detected_class == "CAN" and obj['confidence'] > highest_can['confidence']:
                    highest_can = obj
                elif y2 > self.water_y_threshold and detected_class == "WATER" and obj['confidence'] > highest_tower['confidence']:
                    highest_tower = obj
                elif y2 > self.turn_y_threshold and detected_class == "TURN" and obj['confidence'] > highest_turn['confidence']:
                    highest_turn = obj

            #print(self.objects)
            if highest_can['confidence'] > 0:
                self.Cc_avg = self.shift(self.Cc_avg, highest_can["confidence"])
            else:
                self.Cc_avg = self.shift(self.Cc_avg, 0)
            if highest_tower['confidence'] > 0:
                self.Cw_avg = self.shift(self.Cw_avg, highest_tower["confidence"])
            else:
                self.Cw_avg = self.shift(self.Cw_avg, 0)
            if highest_turn['confidence'] > 0: # there is a turn approaching. find it
                res, min_val, max_val, centre = self.blby.get_blob(self.img)
                turntype = self.leftorright(centre, self.img)
                if turntype == -1:
                    self.Ctl_avg = self.shift(self.Ctl_avg, highest_turn["confidence"])
                elif turntype == 1:
                    self.Ctr_avg = self.shift(self.Ctr_avg, highest_turn["confidence"])
            else:
                self.Ctr_avg = self.shift(self.Ctr_avg, 0)
                self.Ctl_avg = self.shift(self.Ctl_avg, 0)

            Etl = sum(self.Ctl_avg)
            AVGtl = Etl / len(self.Ctl_avg)
            Etr = sum(self.Ctr_avg)
            AVGtr = Etr / len(self.Ctr_avg)
            Ew = sum(self.Cw_avg)
            AVGw = Ew / len(self.Cw_avg)
            Ec = sum(self.Cc_avg)
            AVGc = Ec / len(self.Cc_avg)        
            
            Stl = Etl * AVGtl
            Str = Etr * AVGtr
            Sw  = Ew  * AVGw
            Sc  = Ec  * AVGc
            
            print("Ctl: {:2.5f}, Ctr: {:2.5f}, Cw: {:2.5f}, Cc: {:2.5f}".format(AVGtl, AVGtr, AVGw, AVGc))
            print("Stl: {:2.5f}, Str: {:2.5f}, Sw: {:2.5f}, Sc: {:2.5f}".format(Stl,Str, Sw, Sc))
            
            if Stl > self.turn_threshold:
                print("Turning left")
                self.enter_leftturn()
            if Str > self.turn_threshold:
                print("Turning right")
                self.enter_rightturn()
            if Sw > self.water_threshold:
                print("Avoiding water tower")
                self.enter_watertower()
            if Sc > self.can_threshold:
                print("Getting can")
                self.enter_canpickup()

        if self.lines is None:
            self.enter_stop()
        else:
            # put the current x position of the line straight onto the PID controller
            self.svpid_queue.put(self.lines) # change self.lines to x position -1 is left, 1 is right

    def handle_canpickup(self):
        # check if you haven't found a can. If so stop. look left. look right. etc.
        if self.objects is not None:

            highest_can = {"confidence":0, "type":"CAN"}

            for obj in self.objects:
                x1, x2, y1, y2 = obj['box']
                detected_class = obj['type']
                if y2 > self.can_threshold and detected_class == "CAN" and obj['confidence'] > highest_can['confidence']:
                    highest_can = obj

            if highest_can['confidence'] > 0:
                x1, y1, x2, y2 = highest_can['box']
                centre_x = abs(x1 - x2) / 2
                # x coord to direction
                out = (centre_x * 2) - 1
                self.svpid_queue.put(out)
        else: # no can found
            output = [self.can_search_left[self.can_index], self.can_search_right[self.can_index]]
            self.svpid_queue.put(output)
            self.can_index += 1
            if self.can_index > len(output):
                self.can_index = 0
                # restart the sequence
            
