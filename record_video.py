import cv2 as cv
import time
import os

fps = 10 
w = 1640
h = 1232

def gstreamer_pipeline (capture_width=w, capture_height=h, display_width=w/2, display_height=h/2, framerate=60, flip_method=0) :
    return ('nvarguscamerasrc ! '
    'video/x-raw(memory:NVMM), '
    'width=(int)%d, height=(int)%d, '
    'format=(string)NV12, framerate=(fraction)%d/1 ! '
    'nvvidconv flip-method=%d ! '
    'video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! '
    'videoconvert ! '
    'video/x-raw, format=(string)BGR ! appsink'  % (capture_width,capture_height,framerate,flip_method,display_width,display_height))

folder = "./video_turntest/"
os.mkdir(folder)

cap = cv.VideoCapture(gstreamer_pipeline(flip_method=0), cv.CAP_GSTREAMER)
counter = 0
while True:
    time.sleep(1/fps)
    counter += 1

    ret, image = cap.read()
    print("Capturing frame {}".format(counter))
    cv.imwrite("{}/image{:09}.jpg".format(folder, counter), image)

cap.release()
