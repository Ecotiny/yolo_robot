import json
import serial
import time
import math

ser = serial.Serial("/dev/ttyUSB0")

time.sleep(1)

frequency = 0.1

for i in range(256):
    r = int(math.sin(frequency*i + 0) * 127 + 128)
    g = int(math.sin(frequency*i + 2) * 127 + 128)
    b = int(math.sin(frequency*i + 4) * 127 + 128)
    data = {"left": -1, "r": r, "g": g, "b": b}
    print(r,g,b)
    json_string = json.dumps(data)
    print(json_string)
    ser.write((json_string).encode("utf-8"))
    time.sleep(0.05)
