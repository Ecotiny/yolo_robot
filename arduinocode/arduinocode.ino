#include <Wire.h>
#include <EVShield.h>
#include <ArduinoJson.h>

EVShield          evshield(0x34,0x36);
String inString = "";
StaticJsonDocument<200> doc;

void
setup()
{    
    delay(500);
    Serial.begin(9600);       // start serial for output
    delay(2000);                // wait two seconds, allowing time to
                                // activate the serial monitor
    while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
    }
    evshield.init( SH_HardwareI2C );
    evshield.ledSetRGB(0, 255, 0);
    evshield.waitForButtonPress(BTN_GO, 0);
    Serial.println("READY");
    evshield.ledSetRGB(255, 0, 255);
}

void loop() {
  while (Serial.available() > 0) {
    deserializeJson(doc, Serial);
    // format will be {"left" : 0-100, "right" : 0-100, "r" : 0-255, "g" : 0-255, "b" : 0-255}
    if (doc["r"] >= 0) {
      evshield.ledSetRGB(doc["r"], doc["g"], doc["b"]);
    }
    if (doc["left"] >= 0) {
      evshield.bank_b.motorRunUnlimited(SH_Motor_1, 
                                        SH_Direction_Reverse, 
                                        doc["left"]);
      evshield.bank_a.motorRunUnlimited(SH_Motor_1, 
                                        SH_Direction_Reverse, 
                                        doc["right"]);
    }
  }
}
