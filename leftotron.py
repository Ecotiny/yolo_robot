import numpy as np
from scipy.signal import correlate, convolve

def left(n_samples):
    ret = np.zeros(n_samples)-1
    ret[0:n_samples//2] = 1.0
    return ret

def right(n_samples):
    ret = np.zeros(n_samples)-1
    ret[n_samples//2:] = 1.0
    return ret


class LeftyMcRightface:
    '''
        lr = LeftyMcRightface(N)  # Do this once...

        # Do this for every line (it takes less than one millisecond)

        l,r = lr.get_left_right(data)

    '''
    def __init__(self, N):
        self.n0 = N//12
        self.left = left(self.n0)
        self.N = N

    def x_corr(self, data):
        return correlate(data, self.left, mode='same')

    def get_left_right(self, data):
        assert(len(data) == self.N)

        xcorr = self.x_corr(data - np.mean(data))
        xcorr[0:self.n0//2] = 0
        xcorr[-self.n0//2:] = 0

        i_left = np.argmax(xcorr)
        i_right = np.argmin(xcorr)
        return {"left_edge"      : i_left,
                "right_edge"     : i_right,
                "left_certainty" : xcorr[i_left] / self.n0,
                "right_certainty": -xcorr[i_right] / self.n0}

if __name__=="__main__":
    N = 1000

    # Make some fake data

    data = np.zeros(N)-1

    leftedge = N//3
    rightedge = 2*N//3

    data[0:leftedge] = 1
    data[rightedge:] = 1

    data = data + np.random.normal(0, 1, N)

    lr = LeftyMcRightface(N)
    for i in range(1000):
        l,r = lr.get_left_right(data)
    print("left {}, right {}".format(leftedge, rightedge))
    print("LeftyMcRightface {}".format((l,r)))

    import matplotlib.pyplot as plt

    f, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True)
    ax1.plot(lr.left)
    ax2.plot(data)
    ax3.plot(lr.x_corr(data))
    plt.show()
