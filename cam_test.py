import cv2 as cv
import time

cap = cv.VideoCapture(0)
counter = 0
while True:
    time.sleep(0.1)
    counter += 1

    # RPICAM
    #stream.seek(0)
    #image = stream.array

    # GENERATE EMPTY IMAGES
    #height = 480
    #width = 640
    #image = np.zeros((height,width,3), np.uint8)

    # OPENCV COMPATIBLE WEBCAM
    ret, image = cap.read()
    
    print("displaying image")
    cv.imshow('frame',image)
    if cv.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv.destroyAllWindows()
