import numpy as np
import cv2
from PIL import Image
import time

class Blobby:

    def __init__(self):
        self.template = cv2.imread('blob.png', cv2.IMREAD_COLOR)
        self.t_h, self.t_w, _ = self.template.shape

    def get_blob(self, im):
        ''' Im is an opencv image object

        '''
        h,w,z = im.shape

        res = cv2.matchTemplate(im[h//2:h,:,:], self.template, cv2.TM_SQDIFF_NORMED)


        min_val, max_val, min_loc, _ = cv2.minMaxLoc(res)

        centre = (min_loc[0] + self.t_w//2, min_loc[1] + h//2 + self.t_h//2)

        return res, min_val, max_val, centre

if __name__=="__main__":

    blb = Blobby()

    import os
    directory = './video001/'

    cv2.namedWindow("Detected")
    cv2.namedWindow("Match Result")

    for filename in sorted(os.listdir(directory)):
        if filename.endswith(".jpg"):
            
            img = cv2.imread(directory + filename)
            
            lump = blb.get_blob(img)
            res, min_val, max_val, centre = lump

            cv2.circle(img, centre, 20, 255, 2)

            rgb = img[...,::-1]
            img2 = Image.fromarray(rgb, 'RGB')
            img2.show()
            time.sleep(1)
            img2 = Image.fromarray(res, "L")
            img2.show()
            time.sleep(1)

    cv2.destroyAllWindows()
