import cv2 as cv
import numpy as np

def detect_line(image, roi_height=(300,320), debug=False):
    # double check that height is valid
    if roi_height[0] < roi_height[1] and roi_height[0] > 0 and roi_height[1] < image.shape[0]:
        # height is valid
        valid = True
    else:
        raise ValueError("Incorrect ROI height value!")

    ## convert to greyscale
    grey = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    #grey = image
    # select roi
    roi = grey[300:320, 0:640]

    # for making the max and min values
    #cv.createTrackbar('min','image',0,255,lambda x: x)
    #cv.createTrackbar('max','image',0,255,lambda x: x)

    minimum = 0
    maximum = 32

    edges = cv.Canny(roi, minimum,maximum)


    height, width = edges.shape

    pixel_width = 110

    # Desired "pixelated" size
    w, h = (pixel_width, 1)
    cropped = cv.resize(edges, (w, h), interpolation=cv.INTER_AREA)
    ret,cropped = cv.threshold(cropped,0,255,cv.THRESH_BINARY)
    if debug:
        to_show = cv.resize(cropped, (width, height), interpolation=cv.INTER_NEAREST)

        cv.imshow('roi', roi)
        cv.imshow('edges', edges)
        cv.imshow('results', to_show)

        k = cv.waitKey(0)
        cv.destroyAllWindows()

    # detect consecutive pixels greater than one
    blobs = []
    blobs_i = 0
    last_pixel_flag = 0
    for x in range(w):
        l = cropped[0,x]
        if l > 0 and last_pixel_flag == 0:
            blobs.append([x])
            last_pixel_flag = 1
        elif l > 0 and last_pixel_flag == 1:
            blobs[blobs_i].append(x)
        elif last_pixel_flag == 1:
            blobs[blobs_i] = sum(blobs[blobs_i]) / len(blobs[blobs_i])
            blobs_i += 1
            last_pixel_flag = 0
    # if the last pixel was white
    if last_pixel_flag == 1:
        blobs[blobs_i] = sum(blobs[blobs_i]) / len(blobs[blobs_i])
    for blob_i in range(len(blobs)):
        # turn all into between -1 and 1
        blobs[blob_i] = (blobs[blob_i] - (pixel_width / 2)) / (pixel_width / 2)


    if len(blobs) == 2:
        return blobs
    elif len(blobs) > 2:
        # find two that are closest to 0
        blobs.sort(key=lambda x: abs(x))
        two_closest = blobs[:2]
        return [min(two_closest), max(two_closest)]
    elif debug:
        print("oh frickola, no blobs found!")
        print(blobs)

def leftorright(roi):
    # given an ROI determine whether the left or the right side has more green
    # green mask after converting to hsv
    if 0 in roi.shape:
        return 0
    hsv = cv.cvtColor(roi, cv.COLOR_BGR2HSV)
    gray = cv.cvtColor(hsv, cv.COLOR_BGR2GRAY) 
    ret, mask = cv.threshold(gray,127,255,cv.THRESH_BINARY)#cv.inRange(hsv, (36, 3, 3), (86, 255,200))
    # pixelate down to (2, 1)
    pixelated = cv.resize(mask, (2, 1), interpolation=cv.INTER_LINEAR)
    # save pixelated and mask for debug
    cv.imwrite("orig.jpg", gray)
    cv.imwrite("pixel.jpg", pixelated)
    cv.imwrite("mask.jpg", mask)
    # if left is brighter than right, it is a left turn else right
    if pixelated[0,0] > pixelated[0, 1]:
        return -1
    elif pixelated[0,1] > pixelated[0,0]:
        return 1
    else:
        return 0

# initialize the list of reference points and boolean indicating
# whether cropping is being performed or not
refPt = []
cropping = False

def click_and_crop(event, x, y, flags, param):
    # grab references to the global variables
    global refPt, cropping

    # if the left mouse button was clicked, record the starting
    # (x, y) coordinates and indicate that cropping is being
    # performed
    if event == cv.EVENT_LBUTTONDOWN:
        refPt = [(x, y)]
        cropping = True

    # check to see if the left mouse button was released
    elif event == cv.EVENT_LBUTTONUP:
        # record the ending (x, y) coordinates and indicate that
        # the cropping operation is finished
        refPt.append((x, y))
        cropping = False

        # draw a rectangle around the region of interest
        cv.rectangle(imgin, refPt[0], refPt[1], (0, 255, 0), 2)
        cv.imshow("image", imgin)

if __name__ == "__main__":
    # generate ROI and pass it through leftorright
    imgin = cv.imread("greentest.jpg")

    # load the image, clone it, and setup the mouse callback function
    clone = imgin.copy()
    cv.namedWindow("image")
    cv.setMouseCallback("image", click_and_crop)

    # keep looping until the 'q' key is pressed
    while True:
        # display the image and wait for a keypress
        cv.imshow("image", imgin)
        key = cv.waitKey(1) & 0xFF

        # if the 'r' key is pressed, reset the cropping region
        if key == ord("r"):
            img = clone.copy()

        # if the 'c' key is pressed, break from the loop
        elif key == ord("c"):
            break

    # if there are two reference points, then crop the region of interest
    # from teh image and display it
    print(refPt)
    if len(refPt) == 2:
        roi = clone[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]]
        cv.imshow("ROI", roi)
        print(leftorright(roi))
        cv.waitKey(0)

    # close all open windows
    cv.destroyAllWindows()
