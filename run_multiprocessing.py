import multiprocessing
#import picamera
import time
import io
import process
from leftotron import LeftyMcRightface
import numpy as np
import simple_pid
import argparse
import cv2 as cv
from state_handler import StateHandler
from frameprinter import printframe
import serial
from results import Results
import json

# Camera supervisor starts up camera and makes images from the stream every 10th of a second for ld, half a second for yd
w = 1640
h = 1232

def gstreamer_pipeline (capture_width=w, capture_height=h, display_width=w/2, display_height=h/2, framerate=60, flip_method=0) :
    return ('nvarguscamerasrc ! '
    'video/x-raw(memory:NVMM), '
    'width=(int)%d, height=(int)%d, '
    'format=(string)NV12, framerate=(fraction)%d/1 ! '
    'nvvidconv flip-method=%d ! '
    'video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! '
    'videoconvert ! '
    'video/x-raw, format=(string)BGR ! appsink'  % (capture_width,capture_height,framerate,flip_method,display_width,display_height))

class CameraSupervisor(multiprocessing.Process):

    def __init__(self, ld_queue, yd_queue, sv_queue):
        multiprocessing.Process.__init__(self)
        self.ld_queue = ld_queue
        self.yd_queue = yd_queue
        self.sv_queue = sv_queue
        self.gstreamstring = gstreamer_pipeline()

    def run(self):

        cap = cv.VideoCapture(gstreamer_pipeline(flip_method=0), cv.CAP_GSTREAMER)
        while True:
            time.sleep(0.05)

            ret, image = cap.read()

            if self.yd_queue.empty():
                self.yd_queue.put(image)
            if self.ld_queue.empty():
                self.ld_queue.put(image)
            if self.sv_queue.empty():
                self.sv_queue.put(image)

# LineDetector (detects lines)
class LineDetector(multiprocessing.Process):

    def __init__(self, ld_queue, svld_queue): # line detector queue (in) > supervisor line detector queue (out)
        multiprocessing.Process.__init__(self)
        self.ld_queue = ld_queue
        self.svld_queue = svld_queue

    def run(self):
        lr = LeftyMcRightface(int(round(w/2)))
        import matplotlib.pyplot as plt
        while True:
            time.sleep(0.01)
            if not self.ld_queue.empty():
                img = self.ld_queue.get()

                line_y = int(round(h/2))
                sliceofimg = img[line_y-1:line_y, :]
                sliceofimg = cv.cvtColor(sliceofimg, cv.COLOR_BGR2GRAY)
                sliceofimg = np.divide(sliceofimg, np.amax(sliceofimg))
                results = lr.get_left_right(sliceofimg[0])
                thresh = 0.2

                if results['left_certainty'] > thresh and results['right_certainty'] > thresh:
                    scaled_left = (results['left_edge'] / (w/4)) - 1
                    scaled_right = (results['right_edge'] / (w/4)) - 1
                    offset = (scaled_left + scaled_right) / 2
                    #display = [" "] * 90
                    #display[int(round((offset + 1) * 45))] = "#"
                    #print("".join(display))
                    self.svld_queue.put(offset)
        return

def YOLODetector(yd_queue, svyd_queue, debug=False, debug_queue=False):
    from yolo import YOLO
    settings_dict = {
        "model_path": 'model_data/yolov3-tiny-v2.h5',
        "anchors_path": 'model_data/tiny_yolo_anchors.txt',
        "classes_path": 'model_data/all_classes.txt',
        "score" : 0.3,
        "iou" : 0.45,
        "model_image_size" : (416, 416),
        "gpu_num" : 0,
        "tiny" : True,
    }
    start_modeltime = time.time()
    yolo = YOLO(settings_dict)
    end_modeltime = time.time()
    print("Time taken to build model: {:3.2f}".format(end_modeltime - start_modeltime))
    while True:
        time.sleep(0.01)
        if not yd_queue.empty():
            img = yd_queue.get()

            out_boxes, out_scores, out_classes = yolo.get_boxes(img)
            results = Results(out_boxes, out_scores, out_classes, img)
            objs = results.objects
            if debug:
                #print("I done img")
                # put boxes on in img and put into 'detected' window
                for obj in results.objclasses:
                    obj.draw(img)
                
                cv.line(img, (0, int(round(0.55 * img.shape[0]))), (img.shape[1], int(round(0.55 * img.shape[0]))), (0,0,0), 4)
                debug_queue.put(img)
            
            # print boxes
            #printframe(objs)

            svyd_queue.put(objs)
    return

# supervisor - processes the data from the YOLO and Line detector to choose an X target for the PID controller.
class Supervisor(multiprocessing.Process):

    def __init__(self, sv_queue, svld_queue, svyd_queue, svpid_queue, green_debug_queue): # supervisor - line detector, supervisor - yolo detector, supervisor - PID class
        multiprocessing.Process.__init__(self)
        self.svld_queue = svld_queue
        self.svyd_queue = svyd_queue
        self.sv_queue = sv_queue
        self.statehandler = StateHandler(svpid_queue, w, green_debug_queue)
        self.lines = None # most recent line detector
        self.objects = None # most recent yolo detector object
        self.img = None # most recent img
        self.Ya = 0

    def run(self):

        while True:
            time.sleep(0.05) # twice as fast as images are being put out
            if not self.svld_queue.empty():
                offset = self.svld_queue.get()
                self.lines = offset

            if not self.svyd_queue.empty():
                bounding_boxes = self.svyd_queue.get()
                self.objects = bounding_boxes
                self.Ya = 0
            else:
                self.Ya += 1

            if not self.sv_queue.empty():
                img = self.sv_queue.get()
                self.img = img

            self.statehandler.handle_state(self.lines, self.objects, self.img, self.Ya)



# pid runs pid control loop on x values given by supervisor (or bangbang)
class PID(multiprocessing.Process):

    def __init__(self, svpid_queue, max_throttle, p, i, d): # supervisor - line detector, supervisor - yolo detector, supervisor - PID class
        multiprocessing.Process.__init__(self)
        self.svpid_queue = svpid_queue
        self.pid_ctl = simple_pid.PID(p, i, d, setpoint=0)
        self.max_throttle = max_throttle
        self.prev_lthrottle = 0
        self.prev_rthrottle = 0

    def run(self):
        ser = serial.Serial("/dev/ttyUSB0")
        while True:
            time.sleep(0.01)
            if not self.svpid_queue.empty():
                x_target = self.svpid_queue.get()
                if type(x_target) == float or type(x_target) == int or type(x_target) == np.float64:
                    bangbangthresh = 0.2
                    left_motor = self.max_throttle
                    right_motor = self.max_throttle
                    if x_target > bangbangthresh:
                        left_motor = self.max_throttle
                        right_motor = 0
                    elif x_target < -bangbangthresh:
                        right_motor = self.max_throttle
                        left_motor = 0
                    # turn pid into motor speeds between 0 and 1, then multiply by max_throttle
                    #left_motor = int(round(self.clip(0.5 - pid_result) * self.max_throttle))
                    #right_motor = int(round(self.clip(0.5 + pid_result) * self.max_throttle))
                    data = {"left": left_motor, "right": right_motor, "r" : -1}
                    json_string = json.dumps(data)
                    ser.write((json_string).encode("utf-8"))
                    #print("{: 03} {: 03}".format(left_motor, right_motor))
                    self.prev_rthrottle = right_motor
                    self.prev_lthrottle = left_motor
                elif type(x_target) == list:
                    left_motor, right_motor = x_target
                    data = {"left": left_motor, "right": right_motor, "r" : -1}
                    json_string = json.dumps(data)
                    ser.write((json_string).encode("utf-8"))
                    #print("{: 03} {: 03}".format(left_motor, right_motor))
                    self.prev_rthrottle = right_motor
                    self.prev_lthrottle = left_motor
                elif x_target == "STOP":
                    print("STOPPING")
                    # stop
                    data = {"left": 0, "right": 0, "r" : -1}
                    json_string = json.dumps(data)
                    #print(json_string)
                    ser.write((json_string).encode("utf-8"))
                    #print("{: 03} {: 03}".format(0, 0))
                else:
                    data = {"left": self.prev_lthrottle, "right": self.prev_rthrottle, "r" : -1}
                    json_string = json.dumps(data)
                    #print(json_string)
                    ser.write((json_string).encode("utf-8"))
                    #print("{: 03} {: 03}".format(self.prev_lthrottle, self.prev_rthrottle))


    def clip(self, to_clip): # will compress >1 to 1 and <0 to 0
        if to_clip > 1:
            return 1
        elif to_clip < 0:
            return 0
        return to_clip

def restricted_float(x):
    x = float(x)
    if x < 0.0 or x > 1.0:
        raise argparse.ArgumentTypeError("%r not in range [0.0, 1.0]"%(x,))
    return x


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Run all processes relating to the collection of the can')

    parser.add_argument("max_throttle",
                        metavar="THROTTLE",
                        type=int,
                        choices=range(101),
                        default=0, # no power
                        nargs="?",
                        help="Maximum throttle the PID controller can use. Between 0 and 100. Cruise will be half of this. Default is 0")

    parser.add_argument('--linedetect',
                        dest="ld_count",
                        default=1,
                        type=int,
                        help='Number of separate Line Detector instances to run')

    parser.add_argument('--proportional',
                        default=5,
                        type=float,
                        help='Proportional gain of PID controller')

    parser.add_argument('--integral',
                        default=0.1,
                        type=float,
                        help='Integral gain for PID controller')

    parser.add_argument('--derivative',
                        default=0.05,
                        type=float,
                        help='Derivative gain for PID controller')

    parser.add_argument('--can',
                        default=(1/4),
                        type=restricted_float,
                        help='Width threshold between 0 and 1 the detected can must be wider than for the PID to actually respond to it. Default is 1/4')

    parser.add_argument('--water',
                        default=(1/3),
                        type=restricted_float,
                        help='Width threshold between 0 and 1 the detected water tower must be wider than for the PID to actually respond to it. Default is 1/3')

    parser.add_argument('--turn',
                        default=(2/3),
                        type=restricted_float,
                        help='Width threshold between 0 and 1 the detected turn must be wider than for the PID to actually respond to it. Default is 2/3')
    args = parser.parse_args()

    sv_queue = multiprocessing.Queue()
    ld_queue = multiprocessing.Queue()
    yd_queue = multiprocessing.Queue()
    svld_queue = multiprocessing.Queue()
    svyd_queue = multiprocessing.Queue()
    svpid_queue = multiprocessing.Queue()

    print("Initalising yolo detector")
    # Initialise YOLO Detector
    yd = multiprocessing.Process(target=YOLODetector, args=(yd_queue, svyd_queue,))
    yd.start()

    # initialise camera supervisor
    cam_supervisor = CameraSupervisor(ld_queue, yd_queue, sv_queue)
    cam_supervisor.start()

    print("WAITING")
    time.sleep(40)
    print("STARTING")

    # initialise the supervisor and PID
    supervisor = Supervisor(sv_queue, svld_queue, svyd_queue, svpid_queue)
    supervisor.start()
    PID_ctler = PID(svpid_queue, args.max_throttle, args.proportional, args.integral, args.derivative)
    PID_ctler.start()


    # Initialise Line Detectors
    lds = []
    for i in range(args.ld_count):
        ld = LineDetector(ld_queue, svld_queue)
        lds.append(ld)
        ld.start()

    for ld in lds:
        ld.join()

    yd.join()
    PID_ctler.join()
    supervisor.join()
    cam_supervisor.join()

