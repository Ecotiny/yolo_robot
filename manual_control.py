import curses
import time
import serial
 
# get the curses screen window
screen = curses.initscr()
 
# turn off input echoing
curses.noecho()
 
# respond to keys immediately (don't wait for enter)
curses.cbreak()
 
# map arrow keys to special values
screen.keypad(True)
 
def change_speed(m, l, r):
    m_left, m_right = m
    if m_left + l < 0:
        m_left = 0
    elif m_left + l > 100:
        m_left = 100
    else:
        m_left += l
    
    if m_right + r < 0:
        m_right = 0
    elif m_right + r > 100:
        m_right = 100
    else:
        m_right += r
        
    return [m_left, m_right]

m = [0,0]
deltathrottle = 4

do_serial = True
if do_serial:
    ser = serial.Serial("/dev/ttyUSB0")

try:
    screen.addstr(0,0,"l   r  ")
    while True:
        char = screen.getch()
        if char == ord('q'):
            break
        elif char == curses.KEY_RIGHT:
            m = change_speed(m, deltathrottle, -deltathrottle)
            screen.addstr(2, 0, 'right')
        elif char == curses.KEY_LEFT:
            m = change_speed(m, -deltathrottle, deltathrottle)      
            screen.addstr(2, 0, 'left ')
        elif char == curses.KEY_UP:
            m = change_speed(m, deltathrottle, deltathrottle)
            screen.addstr(2, 0, 'up   ')
        elif char == curses.KEY_DOWN:
            m = change_speed(m, -deltathrottle, -deltathrottle)
            screen.addstr(2, 0, 'down ')
        elif char == ord("x"): # kill throttle
            m = change_speed(m, -100, -100)
            screen.addstr(2, 0, 'kill ')
        elif char == ord("z"): # max throttle
            m = change_speed(m, 100, 100)
            screen.addstr(2, 0, 'max  ')
        screen.addstr(1,0, "{:03d} {:03d}".format(m[0], m[1]))
        if do_serial:
            ser.write((str(m[0]) + "l").encode('utf-8'))
            ser.write((str(m[1]) + "r").encode('utf-8'))
        
        #time.sleep(0.05)
finally:
    # shut down cleanly
    curses.nocbreak(); screen.keypad(0); curses.echo()
    curses.endwin()
